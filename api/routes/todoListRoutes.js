'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/todoListController');

  // todoList Routes
  app.route('/sensors')
    .get(todoList.list_all_sensor_value)
    .post(todoList.create_a_sensor_value);


  app.route('/sensors/:sensorId')
    .get(todoList.list_a_value)
    .put(todoList.update_a_value)
    .delete(todoList.delete_a_value);
    
  app.route('/graph').get(todoList.plotGraph);
  
  app.route('/data.tsv').get(todoList.getData);
  
  app.route('/graphtest').get(todoList.plotGraphTest);

  app.route('/ledstrip').get(todoList.ledStripController);
  
  app.route('/ledstripget').post(todoList.ledStripColorGet);
  
  app.route('/ledcolor').get(todoList.ledStripColor);
  
  app.route('/firmware').get(todoList.firmware);
  
  app.route('/fw').get(todoList.redirect);
};