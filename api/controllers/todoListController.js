'use strict';
var MongoClient = require('mongodb').MongoClient;
var http = require('http');
var fs = require('graceful-fs');
var url = require('url');
var path = require('path');
var root = path.dirname(require.main.filename);


var mongoose = require('mongoose'),
        Sensor = mongoose.model('Sensors');

exports.list_all_sensor_value = function (req, res) {
    Sensor.find({}, function (err, list) {
        if (err)
            res.send(err);
        res.json(list);
    });
};




exports.create_a_sensor_value = function (req, res) {
    var new_task = new Sensor(req.body);
    new_task.save(function (err, sensor_value) {
        if (err)
            res.send(err);
        res.json(sensor_value);
    });
};


exports.list_a_value = function (req, res) {
    Sensor.findOne({name: req.params.sensorId}, null, {sort:{$natural:-1}}, function (err, sensor_value) {
        if (err)
            res.send(err);
        res.json(sensor_value);
    });
};


exports.update_a_value = function (req, res) {
    Sensor.findOneAndUpdate({_id: req.params.taskId}, req.body, {new : true}, function (err, sensor_value) {
        if (err)
            res.send(err);
        res.json(sensor_value);
    });
};


exports.delete_a_value = function (req, res) {

    Sensor.remove({
        _id: req.params.taskId
    }, function (err, sensor_value) {
        if (err)
            res.send(err);
        res.json({message: 'Sensor successfully deleted'});
    });
};

exports.plotGraph = function (req, res) {

    MongoClient.connect("mongodb://localhost:27017/aquarium", function (err, db) {

        db.collection('sensors', function (err, collection) {

            collection.find({}, {_id: false, name: true, value: true, created_date: true}, function (err, cursor) {
                if (err) {
                    console.log(err);
                }
                var dataWriteStream = fs.createWriteStream("data.tsv");
                dataWriteStream.write("date\tclose\r\n");
                cursor.each(function (err, item) {
                    if (item) {
                        //console.log(item);
                        //console.log(Date.parse(item.created_date) + "\t" + item.value);
                        dataWriteStream.write(Date.parse(item.created_date) + "\t" + item.value + "\r\n");
                    } else {
                        console.log("The file was saved!");
                        fs.readFile("graph.html", function (err, data) {
                            if (err) {
                                console.log(err);
                                res.writeHead(404, {'Content-Type': 'text/html'});
                            } else {
                                res.writeHead(200, {'Content-Type': 'text/html'});
                                res.write(data.toString());
                            }
                            res.end();
                        });
                    }

                })
            });
        });
    });
}


exports.getData = function (req, resp) {
    fs.readFile("data.tsv", function (err, data) {
        if (err) {
            console.log(err);
            resp.writeHead(404, {'Content-Type': 'text/html'});
        } else {
            resp.writeHead(200, {'Content-Type': 'text/html'});
            resp.write(data.toString());
        }
        resp.end();
    });
}

exports.plotGraphTest = function (req, resp) {
    fs.readFile("graph.html", function (err, data) {
        if (err) {
            console.log(err);
            resp.writeHead(404, {'Content-Type': 'text/html'});
        } else {
            resp.writeHead(200, {'Content-Type': 'text/html'});
            resp.write(data.toString());
        }
        resp.end();
    });
}

exports.ledStripController = function(req, resp){
     var html='';
  html +="<body>";
  html += "<form action='/ledstripget'  method='post' name='form1'>";
  html += "Color:</p><input type= 'text' name='color'>";
  html += "<input type='submit' value='submit'>";
  html += "<INPUT type='reset'  value='reset'>";
  html += "</form>";
  html += "</body>";
  resp.send(html);
}

exports.ledStripColorGet = function(req, resp){
  var reply='';
  reply += "The led strip color is " + req.body.color;
  resp.send(reply);
  
  var dataWriteStream = fs.createWriteStream("stripcolor.html");
  var html='';
  //html +="<body>";
  html += req.body.color;
  //html += "</body>";
  dataWriteStream.write(html);
}

exports.ledStripColor = function (req, resp) {
    fs.readFile("stripcolor.html", function (err, data) {
        if (err) {
            console.log(err);
            resp.writeHead(404, {'Content-Type': 'text/html'});
        } else {
            resp.writeHead(200, {'Content-Type': 'text/html'});
            resp.write(data.toString());
        }
        resp.end();
    });
}

exports.firmware = function (req, resp) {
    fs.readFile("firmware.txt", function (err, data) {
        if (err) {
            console.log(err);
            resp.writeHead(404, {'Content-Type': 'text/html'});
        } else {
            resp.writeHead(200, {'Content-Type': 'text/html'});
            resp.write(data.toString());
        }
        resp.end();
    });
}

exports.redirect = function(req, resp){
	  const options = {
            port: 3000,
            hostname: '201.13.33.146',
            method: 'GET',
            path: '/firmware'
         };
		 var reqForward = http.request(options, (newResponse) => {
                //Do something with your newResponse
                var responseData = "";
                newResponse.on('data', (chunk) => {
                    //Add data response from newResponse
					console.log('data:' + chunk);
                    responseData += chunk;
                });

                newResponse.on('end', () => {
                    //Nothing more, send it with your original Response
                    resp.send(responseData);
                });
        });
		     // If ERROR
        reqForward.on('error', (e) => {
             console.error('Error: ' + e);
        });
		
		       // Write to the request
        //reqForward.write("");
        reqForward.end();
}
