'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var sensorSchema = new Schema({
    name: {
        type: String,
        required: 'Kindly enter the name of the task'
    },
    created_date: {
        type: Date,
    default: Date.now
    },
    sensor_type: {
        type: String,
        required: 'Kindly enter the type of sensor'
    },
    value: {
        type: Number,
        required: 'Kindly enter the value read from sensor'
    }
});

module.exports = mongoose.model('Sensors', sensorSchema);