var express = require('express'),
  app = express(),
 // port = process.env.PORT || 3050,
 //port = 3000;
 port = 80;
  mongoose = require('mongoose'),
  Task = require('./api/models/todoListModel'), //created model loading here
  bodyParser = require('body-parser');
  
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://felipeghleite:fe427042@ds223763.mlab.com:23763/fishtanklogger');
//mongoose.connect('mongodb://felipeghleite:fe427042@ds040017.mlab.com:40017/flwebsite-mongodb'); 

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route

app.listen(port);

app.use(function(req, res) {
 res.status(404).send({url: req.originalUrl + ' not found'})//
});

console.log('todo list RESTful API server started on: ' + port);
